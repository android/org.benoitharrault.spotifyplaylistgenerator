import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:spotifyplaylistgenerator/config/application_config.dart';
import 'package:spotifyplaylistgenerator/cubit/activity/activity_cubit.dart';
import 'package:spotifyplaylistgenerator/ui/skeleton.dart';

void main() async {
  // Initialize packages
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  final Directory tmpDir = await getTemporaryDirectory();
  Hive.init(tmpDir.toString());
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: tmpDir,
  );

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((value) => runApp(EasyLocalization(
            path: 'assets/translations',
            supportedLocales: const <Locale>[
              Locale('en'),
              Locale('fr'),
            ],
            fallbackLocale: const Locale('en'),
            useFallbackTranslations: true,
            child: const MyApp(),
          )));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        // default providers
        BlocProvider<NavCubitPage>(
          create: (context) => NavCubitPage(appConfig: ApplicationConfig.config),
        ),
        BlocProvider<NavCubitScreen>(
          create: (context) => NavCubitScreen(),
        ),
        BlocProvider<ApplicationThemeModeCubit>(
          create: (context) => ApplicationThemeModeCubit(),
        ),
        BlocProvider<ActivityCubit>(
          create: (context) => ActivityCubit(),
        ),
        BlocProvider<ActivitySettingsCubit>(
          create: (context) => ActivitySettingsCubit(appConfig: ApplicationConfig.config),
        ),
      ],
      child: BlocBuilder<ApplicationThemeModeCubit, ApplicationThemeModeState>(
        builder: (BuildContext context, ApplicationThemeModeState state) {
          return MaterialApp(
            title: ApplicationConfig.config.appTitle,
            home: const SkeletonScreen(),

            // Theme stuff
            theme: lightTheme,
            darkTheme: darkTheme,
            themeMode: state.themeMode,

            // Localization stuff
            localizationsDelegates: context.localizationDelegates,
            supportedLocales: context.supportedLocales,
            locale: context.locale,
            debugShowCheckedModeBanner: false,
          );
        },
      ),
    );
  }
}
