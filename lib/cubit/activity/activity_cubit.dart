import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:spotifyplaylistgenerator/models/activity/activity.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createNull(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isConnected: state.currentActivity.isConnected,
      // Base data
      userId: state.currentActivity.userId,
      // Activity data
      token: state.currentActivity.token,
    );
    // activity.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);
    refresh();
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isConnected = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isConnected = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isConnected = false;
    refresh();
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}
