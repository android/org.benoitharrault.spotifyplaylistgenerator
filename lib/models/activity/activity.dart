import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:spotifyplaylistgenerator/config/application_config.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = true,
    this.isConnected = false,

    // Base data
    this.userId = '',

    // Activity data
    this.token = 1,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isConnected;

  // Base data
  String userId;

  // Activity data
  int token;

  factory Activity.createNull() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      userId: '',
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isConnected: false,
      // Base data
      userId: '',
    );
  }

  bool get canBeResumed => isConnected;

  void dump() {
    printlog('');
    printlog('## Current activity dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isConnected: $isConnected');
    printlog('  Base data');
    printlog('    userId: $userId');
    printlog('  Activity data');
    printlog('    token: $token');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isConnected': isConnected,
      // Base data
      'userId': userId,
      // Activity data
      'token': token,
    };
  }
}
