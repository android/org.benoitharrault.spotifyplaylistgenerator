import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:spotifyplaylistgenerator/cubit/activity/activity_cubit.dart';
import 'package:spotifyplaylistgenerator/models/activity/activity.dart';

class PagePlaylist extends StatelessWidget {
  const PagePlaylist({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const SizedBox(height: 8),
            const Text('[playlist]'),
            Text(currentActivity.toString()),
          ],
        );
      },
    );
  }
}
