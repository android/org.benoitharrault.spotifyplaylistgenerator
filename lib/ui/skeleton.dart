import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:spotifyplaylistgenerator/config/application_config.dart';
import 'package:spotifyplaylistgenerator/cubit/activity/activity_cubit.dart';
import 'package:spotifyplaylistgenerator/models/activity/activity.dart';

class SkeletonScreen extends StatelessWidget {
  const SkeletonScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavCubitScreen, int>(
      builder: (BuildContext context, int screenIndex) {
        return BlocBuilder<ActivityCubit, ActivityState>(
          builder: (BuildContext context, ActivityState activityState) {
            return BlocBuilder<NavCubitPage, int>(
              builder: (BuildContext context, int pageIndex) {
                final Activity currentActivity = activityState.currentActivity;

                // autostart activity
                if (ApplicationConfig.config.autoStartActivity &&
                    (!currentActivity.isRunning)) {
                  ApplicationConfig.config.startNewActivity(context);
                }

                return Scaffold(
                  appBar: GlobalAppBar(
                    appConfig: ApplicationConfig.config,
                    pageIndex: pageIndex,
                    isActivityRunning: currentActivity.isConnected,
                  ),
                  extendBodyBehindAppBar: false,
                  body: Material(
                    color: Theme.of(context).colorScheme.surface,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 8,
                        left: 2,
                        right: 2,
                      ),
                      child: ApplicationConfig.config.navigation.getScreenWidget(
                        appConfig: ApplicationConfig.config,
                        screenIndex: screenIndex,
                      ),
                    ),
                  ),
                  backgroundColor: Theme.of(context).colorScheme.surface,
                  bottomNavigationBar: ApplicationConfig.config.navigation.displayBottomNavBar
                      ? BottomNavBar(appConfig: ApplicationConfig.config)
                      : null,
                );
              },
            );
          },
        );
      },
    );
  }
}
